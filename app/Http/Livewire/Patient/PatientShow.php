<?php

namespace App\Http\Livewire\Patient;

use App\Models\BloodPressure;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class PatientShow extends Component
{
    public $patient,$blood_pressure;
    protected $listeners=[
        'blood_record_added'=>'render',
    ];
    public function render()
    {
        return view('livewire.patient.patient-show');
    }
    protected $rules = [
        'blood_pressure' => 'required|numeric|min:0|max:200',
    ];

    public function save_blood_record()
    {
        $this->validate();
        BloodPressure::query()->create([
                'blood_pressure'=>$this->blood_pressure,
                'patient_id'=>$this->patient->id,
                'user_id'=>Auth::user()->id,
            ]
        );
        $this->resetInputFields();
        $this->emitTo('patient.patient-blood-table','refresh_patient_blood_table',['patient_id'=>$this->patient->id]);
      //  $this->emit('blood_record_added','Blood record added successfully');
    }
    private function resetInputFields()
    {
        $this->blood_pressure = '';
    }
}
