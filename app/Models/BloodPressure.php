<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BloodPressure extends Model
{
    use HasFactory;
    protected $fillable=[
        'blood_pressure',
        'recorded_at',
        'patient_id',
        'user_id'
    ];

    public function recorded_by(){
        return $this->belongsTo(User::class,'user_id');
    }
}
