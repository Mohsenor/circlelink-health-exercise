<?php

namespace App\Http\Livewire\Patient;

use App\Exports\PatientsExport;
use App\Models\Patient;
use Livewire\Component;
use Maatwebsite\Excel\Facades\Excel;

class PatientCreate extends Component
{
    public $first_name,$last_name,$email,$phone,$address,$description;
    public $addpatient = false;
    public function render()
    {
        return view('livewire.patient.patient-create');
    }
    protected $rules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required|email|unique:patients',
        'phone' => 'required|unique:patients',
        'address' => 'required',
        'description' => 'required',
    ];
    private function resetInputFields(){
        $this->first_name = '';
        $this->last_name = '';
        $this->email = '';
        $this->address = '';
        $this->phone = '';
        $this->description ='';
    }
    public function submit()
    {
        $this->validate();
        Patient::create([
            'first_name'=>$this->first_name,
            'last_name'=>$this->last_name,
            'email'=>$this->email,
            'phone'=>$this->phone,
            'address'=>$this->address,
            'description'=>$this->description
        ]);
        $this->resetInputFields();
        $this->emitTo('patient.patient-table','refresh_patient_table');
    }

    public function addpatient()
    {
        $this->addpatient = true;
    }

    public function export()
    {
        try {
            ini_set('memory_limit', '-1');
            return Excel::download(new PatientsExport(), 'patients_' . date('Y-m-d H:i:s') . '.xlsx');
        } finally{
            $this->notify('CSV file downloaded');
            $this->dispatchBrowserEvent('alert');
        }
    }
}
