<h2>Minimum Php Version 7.4</h2>
<h3>Follow these instructions to run the project</h3>
run cp .env.example .env and setup your .env<br>
composer install <br>
npm install <br>
npm run dev <br>
php artisan key:generate <br>
php artisan migrate --seed <br>
php artisan serve <br>

<h3>Log in using :</h3>
 email: admin@admin.com<br>
 password: password
<h3>For the tests</h3>
php artisan test
