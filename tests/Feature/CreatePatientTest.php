<?php

namespace Tests\Feature;

use App\Exports\PatientsExport;
use App\Http\Livewire\Patient\PatientCreate;
use App\Models\Patient;
use App\Models\User;
use Livewire\Livewire;
use Maatwebsite\Excel\Facades\Excel;
use Tests\TestCase;

class CreatePatientTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create_patient()
    {
        $this->actingAs($user = User::factory()->create());
        $patient = Patient::factory()->make();
        Livewire::test(PatientCreate::class)
            ->set('first_name', $patient->first_name)
            ->set('last_name', $patient->last_name)
            ->set('address', $patient->address)
            ->set('phone', $patient->phone)
            ->set('email', $patient->email)
            ->set('description', $patient->description)
            ->call('submit');
        $this->assertTrue(Patient::where('phone','=',$patient->phone)->exists());
        Patient::where('phone','=',$patient->phone)->delete();
        $user->forceDelete();
    }

    function test_find_create_patient_component()
    {
        $this->actingAs($user = User::factory()->create());
        $this->get('/patients/index')->assertSeeLivewire('patient.patient-create');
        $user->forceDelete();
    }

    function test_first_name_is_required()
    {
        $this->actingAs($user=User::factory()->create());
        Livewire::test(PatientCreate::class)
            ->set('first_name', '')
            ->call('submit')
            ->assertHasErrors(['first_name' => 'required']);
        $user->delete();
    }

    function test_can_download_patients_csv(){
        $this->actingAs($user=User::factory()->create());
        Livewire::test(PatientCreate::class)
            ->call('export')
            ->assertDispatchedBrowserEvent('alert');
        $user->delete();
    }
}
