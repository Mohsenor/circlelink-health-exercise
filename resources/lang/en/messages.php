<?php

return [

    'blood_pressure' => 'Blood pressure',
    'first_name' => 'First name',
    'last_name' => 'Last name',
    'email' =>'Email',
    'description' => 'Description',
    'phone' => 'Phone',
    'address' => 'Address'
];
