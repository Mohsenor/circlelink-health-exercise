<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;

    protected $table= 'patients';
    protected $fillable=[
        'first_name',
        'last_name',
        'email',
        'phone',
        'description',
        'address'
    ];

    public function blood_pressure_records(){
        return $this->hasMany(BloodPressure::class,'patient_id');
    }
}
