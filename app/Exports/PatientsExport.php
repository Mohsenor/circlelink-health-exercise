<?php

namespace App\Exports;

use App\Models\Patient;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;

class PatientsExport implements FromQuery
{

    public function query()
    {
        return Patient::query()->select(['first_name','last_name','email','phone','address','description']);
    }
}
