<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Patients
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-8xl mx-auto sm:px-6 lg:px-8">
             @if(session()->has('patient-not-found'))
                <div
                    class="flex justify-between text-orange-200 shadow-inner rounded p-3 bg-orange-600"
                >
                    <p class="self-center"><strong>Warning </strong>{{session()->pull('patient-not-found')}}</p>
                    <strong class="text-xl align-center cursor-pointer alert-del"
                    >&times;</strong
                    >
                </div><br>
             @endif
            @livewire('patient.patient-create')
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                @livewire('patient.patient-table')
            </div>
        </div>
    </div>
</x-app-layout>
