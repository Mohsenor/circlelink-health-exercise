<?php

namespace App\Http\Controllers;

use App\Models\Patient;
use Illuminate\Support\Facades\Log;

class PatientController extends Controller
{
    public function index()
    {
        return view('patients.index');
    }

    public function show($id){
        try {
            $patient = Patient::findorfail($id);
        }catch (\Exception $exception){
            Log::error($exception);
            return redirect()->route('patients.index')->with(['patient-not-found'=>'We couldn\'t find this patient']);
        }
        return view('patients.show', compact('patient'));
    }
}
