<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => '$2y$10$WilK4uMjf6X7Eo8C6IBGeu7DAB3pXrBgZcxqnZonNoooOuQC//zc2',
                'role_id'        => '1',
                'remember_token' => null,
            ],
        ];
        User::insert($users);
    }
}
